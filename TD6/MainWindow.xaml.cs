﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TD6
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Line line = new Line();
        public MainWindow()
        {
            InitializeComponent();
           
        }

        

        private void UnCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {

            if (!(this.Color.SelectedItem is ComboBoxItem comboBoxItem)) return;
            if (!(comboBoxItem.Content is StackPanel stackPanel)) return;
            line.Stroke = (stackPanel.Children[0] as Rectangle)?.Fill;

            Point MousePosition = Mouse.GetPosition(UnCanvas);

            if (line.X1 == 0 && line.Y1 == 0) {
            line.X1 = MousePosition.X;
            line.Y1 = MousePosition.Y;
            } else {
            line.X2 = MousePosition.X;
            line.Y2 = MousePosition.Y;

            UnCanvas.Children.Add(line);
            line = new Line();
            line.X1 = MousePosition.X;
            line.Y1 = MousePosition.Y;
            }
        }

        private void Btn_clear_Click(object sender, RoutedEventArgs e)
        {
            UnCanvas.Children.Clear();
            line=new Line();
        }
    }
}
